// jQuery shit
"use strict";
(function() {
    $(document).ready(function () {
        $("#errorfield").hide();
        $("#calcbtn").click(function () {
            setInputValue();
            changeZero();
            setNoZeroPrimes();
            showAllPrimes();
            setPrimes();
        });
        $("#showmoreprimes").click(function () {
            showMore();
            scrollDown();
        });
        $("#scrolltotop").click(function () {
            scrollTop();
        })
    });

    let noZeroPrimes = [];
    let primes = [];
    let indexToOutPutFrom = 0;
    let inputValue = 0;

    function setInputValue() {
        if (isNumeric($("#textField").val())) {
            inputValue = $("#textField").val();
            $("#errorfield").hide();
        } else
            $("#errorfield").text("Input not a number!").show();
    }

    function setNoZeroPrimes() {
        if ( $("#textField").val() > 0) {
            noZeroPrimes = MakePrimeList.getPrimes(inputValue);
            $("#showmoreprimes").show();
        }
    }

    function setPrimes() {
        cleanUp();
        $("#reveal").text(" ");
        primes = MakePrimes.primes(noZeroPrimes, inputValue);
        $("#reveal").text(valuesFromArray(primes,0, primes.length).slice(0, -2));
        $("#showmoreprimes").show();
    }

    function showMore() { // I can't get it to 5 lines :(
        let str = valuesFromArray(noZeroPrimes, indexToOutPutFrom, indexToOutPutFrom + 40);
        if (indexToOutPutFrom >= noZeroPrimes.length) {
            $("#primelist").text($("#primelist").text().slice(0, -2));
            $("#showmoreprimes").hide();
        } else {
            $("#primelist").text($("#primelist").text()+str);
            indexToOutPutFrom += 40;
        }
    }

    function valuesFromArray(array, fromIndex, toIndexExclusive) {
        let string = "";
        for (let i = fromIndex; i < toIndexExclusive; i++) {
            if (array[i] != undefined) {
                string += (array[i] + ", ");
            }
        }
        return string;
    }

    function cleanUp() {
        indexToOutPutFrom = 0;
        $("#primelist").text("");

    }

    function scrollDown() {
        window.scrollBy(0,200);
    }

    function scrollTop() {
        window.scrollTo(0, 0);
    }
    function changeZero() {
        $("#changezero1").text(inputValue);
        $("#changezero2").text(inputValue);
    }

    function showAllPrimes() {
        $("#showallprimes").text("\xa0\xa0\xa0" + noZeroPrimes.length);
    }

    function isNumeric(value) {
        return !isNaN(parseFloat(value)) && isFinite(value);
    }
})();

