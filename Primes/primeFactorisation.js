// You didn't see anything... cause there's nothing here
"use strict";

let MakePrimes = (function() {

    // Makes no sense to cut this one up into little functions

    function calculatePrimes(primesArray, value) {
        let primes = [];
        let i = 0;
        while (value >= primesArray[i]) {
            if (value % primesArray[i] == 0) {
                primes.push(primesArray[i]);
                value /= primesArray[i];
            } else {
                i++;
            }
        }

        return primes;
    }
    return { primes: calculatePrimes }
})();
