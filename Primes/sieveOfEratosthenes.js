// In this file you can find the sieve of Eratosthenes, and that's really all you need to know and also all I know
"use strict";

let MakePrimeList = (function () {
    let primes = [];
    let counter = 2;

    function sieve(number) {
        fillNumberArray(primes, 0, number);
        for (let i = 2; i < primes.length; i++) {
            if (primes[i] != 0) {
                setIndexToVal(primes, counter, 0, primes[i]);
                counter = 2;
            }
        }
        return filterArrayByValue(primes, 1);
    }

    function fillNumberArray(array, startNumber, toNumberInclusive) {
        for (let i = startNumber; i <= toNumberInclusive; i++) {
            array[i] = i;
        }
        return array;
    }

    function setIndexToVal(array, counter, value, optionalMultiplier) {
        while (counter * optionalMultiplier < array.length) {
            array[counter * optionalMultiplier] = value;
            counter++;
        }
    }

    function filterArrayByValue(array, greaterThanValue) {
        return array.filter(function (value) {
            return (value > greaterThanValue)
        })
    }
    return { getPrimes: sieve }
})();


